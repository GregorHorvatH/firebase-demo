// core
import React, { Component } from 'react';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/messaging';

// components
import Auth from '../../components/Auth';
import Header from '../../components/Header';
import Messages from '../../components/Messages';

import { firebaseConfig, usePublicVapidKey } from '../../firebaseConfig';

class App extends Component {
  state = {
    isAuthorized: false,
    isLoading: true,
    user: {},
    messages: [],
  }

  componentDidMount() {
    firebase.initializeApp(firebaseConfig);
    this.db = firebase.firestore();
    this.messaging = firebase.messaging();
    this.messaging.usePublicVapidKey(usePublicVapidKey);

    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.

        // Get Instance ID token. Initially this makes a network call, once retrieved
        // subsequent calls to getToken will return from cache.
        this.messaging.getToken().then((currentToken) => {
          if (currentToken) {
            // sendTokenToServer(currentToken);
            // updateUIForPushEnabled(currentToken);
            console.log(currentToken);
          } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            // updateUIForPushPermissionRequired();
            // setTokenSentToServer(false);
          }
        }).catch((err) => {
          console.log('An error occurred while retrieving token. ', err);
          // showToken('Error retrieving Instance ID token. ', err);
          // setTokenSentToServer(false);
        });

        // Callback fired if Instance ID token is updated.
        this.messaging.onTokenRefresh(() => {
          this.messaging.getToken().then((refreshedToken) => {
            console.log('Token refreshed.', refreshedToken);
            // Indicate that the new Instance ID token has not yet been sent to the
            // app server.
            // setTokenSentToServer(false);
            // Send Instance ID token to app server.
            // sendTokenToServer(refreshedToken);
            // ...
          }).catch((err) => {
            console.log('Unable to retrieve refreshed token ', err);
            // showToken('Unable to retrieve refreshed token ', err);
          });
        });

        this.messaging.onMessage((payload) => {
          console.log('Message received. ', payload);
          // ...
        });

      } else {
        console.log('Unable to get permission to notify.');
      }
    });

    this._observeUser();
  }

  componentWillUnmount() {
    if (this.unsubscribeMessages) {
      this.unsubscribeMessages();
    }
  }

  _enableLoader = () => this.setState({ isLoading: true });
  _disableLoader = () => this.setState({ isLoading: false });

  _observeUser = () => {
    firebase.auth()
      .onAuthStateChanged((user) => {
        if (user) {
          console.log(user.toJSON());

          this.setState({
            isAuthorized: true,
            user: user.toJSON(),
          });

          this.unsubscribeMessages = this._observeMessges();
        } else {
          this.setState({
            isAuthorized: false,
            user: {},
          });
        }
        this._disableLoader();
      });
  }

  _observeMessges = () => {
    this.db.collection("messages")
      .orderBy("timestamp", "desc")
      .onSnapshot((data) => {
        const messages = [];

        data.docs.forEach(doc => messages.push({
          id: doc.id,
          ...doc.data()
        }));
        this.setState({ messages });
      });
  }

  _signOut = () => {
    firebase.auth()
      .signOut()
      .then(() => {
        this.setState({
          isAuthorized: false,
          user: {},
        });
      })
      .catch(error => console.log(error))
      .finally(this._disableLoader);
  };

  _signInWithEmailAndPassword = () => {
    const email = 'test.user@gmail.com';
    const password = '123456';

    firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then(data => console.log(data))
      .catch(error => console.log(error))
      .finally(this._disableLoader);
  };

  _signInWithGoogle = () => {
    firebase.auth()
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(data => console.log(data))
      .catch(error => console.log(error))
      .finally(this._disableLoader);
  };

  _signInWithFacebook = () => {
    firebase.auth()
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(data => console.log(data))
      .catch(error => console.log(error))
      .finally(this._disableLoader);
  };

  _createNewUser = () => {
    const email = 'test.user@gmail.com';
    const password = '123456';

    firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(res => console.log(res && res.user.toJSON()))
      .catch(error => console.log(error))
      .finally(this._disableLoader);
  };

  _handleSubmit = (param) => {
    this._enableLoader();

    switch (param) {
      case 0:
        this._signOut();
        return;
      case 1:
        this._signInWithEmailAndPassword();
        return;
      case 2:
        this._signInWithGoogle();
        return;
      case 3:
        this._signInWithFacebook();
        return;
      case 4:
        this._createNewUser();
        return;
      default:
        this._disableLoader();
        console.log('Sign In Provider is not found!');
    }
  }

  _handleAddMessage = (text) => {
    console.log(text);

    this.db.collection("messages")
      .add({
        timestamp: new Date().getTime(),
        email: this.state.user.email,
        text,
      })
      .then(docRef => console.log("Document written with ID: ", docRef.id))
      .catch(error => console.error("Error adding document: ", error));
  }

  render() {
    const { isAuthorized, isLoading, messages, user } = this.state;

    return (
      <div className="container app">
        <Header user={user} />
        <Auth
          isLoading={isLoading}
          isAuthorized={isAuthorized}
          onSubmit={this._handleSubmit}
        />
        {
          isAuthorized &&
          <Messages
            list={messages}
            onAddMessage={this._handleAddMessage}
          />
        }
      </div>
    );
  }
}

export default App;
