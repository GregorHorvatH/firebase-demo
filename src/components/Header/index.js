// core
import React, { Fragment } from 'react';

// styles
import './styles.scss';

import logo from '../../assets/fb-logo.png';
import noUser from '../../assets/no_image_user_profile.png';

const Header = ({ user }) => (
  <Fragment>
    <div className="row justify-content-center">
      <div className="header col-sm-12 col-md-6 text-center">
        <img src={logo} alt="firebase logo" />
        <h2>FireBase Demo</h2>
      </div>
    </div>
    {
      user && user.email &&
      <div className="row justify-content-center">
        <div className="user-info col-sm-12 col-md-6 text-center">
          <img src={user.photoURL || noUser} alt="user" />
          <h5>{user.displayName || 'no name'}</h5>
          <a href={`mailto:${user.email}`}>{user.email || 'no email'}</a>
        </div>
      </div>
    }
  </Fragment>
);

export default Header;
