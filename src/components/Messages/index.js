// core
import React, { Fragment, useState } from 'react';

// styles
import './styles.scss';

const Messages = ({ list, onAddMessage }) => {
  const [text, setText] = useState('');

  return (
    <Fragment>
      <div className="row justify-content-center">
        <div className="col-sm-9 col-md-7">
          <form onSubmit={(e) => {
            e.preventDefault();
            onAddMessage(text);
            setText('');
          }}>
            <div className="input-group">
              <input
                className="form-control"
                type="text" value={text}
                onChange={e => setText(e.target.value)}
              />
              <div className="input-group-append">
                <button type="submit" className="btn btn-dark">+ Add Message</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div className="row justify-content-center">
        <div className="col-sm-9 col-md-7">
          {
            list.map(({ id, text, email }) => (
              <div key={id} className="card">
                <p><strong>Text: </strong>{text}</p>
                <p><strong>Email: </strong>{email}</p>
              </div>
            ))
          }
        </div>
      </div>
    </Fragment>
  );
};

export default Messages;
