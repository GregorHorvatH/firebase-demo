// core
import React, { Fragment } from 'react';

const Buttons = ({ isAuthorized, onSubmit }) =>
  isAuthorized
    ? <Fragment>
      <button className="btn btn-primary btn-block"
        onClick={() => onSubmit(0)}>Sign Out</button>
    </Fragment>

    : <Fragment>
      <button className="btn btn-info btn-block"
        onClick={() => onSubmit(1)}>Email And Password</button>

      <button className="btn btn-success btn-block"
        onClick={() => onSubmit(2)}>Google</button>

      <button className="btn btn-primary btn-block"
        onClick={() => onSubmit(3)}>Facebook</button>

      <hr />
      <button className="btn btn-warning btn-block"
        onClick={() => onSubmit(4)}>Create New User</button>
    </Fragment>;

const Auth = ({ isLoading, isAuthorized, onSubmit }) => (
  <div className="row justify-content-center">
    <div className="col-sm-9 col-md-7 text-center">
      {
        isLoading
          ? <h3>Loading...</h3>
          : <Buttons isAuthorized={isAuthorized}
            onSubmit={onSubmit}
          />
      }
    </div>
  </div>
);

export default Auth;
